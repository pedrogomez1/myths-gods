package com.psgs.mythsgods.service;

import com.psgs.mythsgods.entity.Gods;

import java.util.List;

public interface IGodsService {

    public Gods get (Integer id);
    public List<Gods> getAll();
    public void post(Gods gods);
    public void put(Gods gods, int id);
    public void delete(int id);

}
