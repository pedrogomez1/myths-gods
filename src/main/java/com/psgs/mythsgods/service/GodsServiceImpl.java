package com.psgs.mythsgods.service;

import com.psgs.mythsgods.client.IOriginsClient;
import com.psgs.mythsgods.dao.IGodsDao;
import com.psgs.mythsgods.entity.Gods;
import com.psgs.mythsgods.model.Origins;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GodsServiceImpl implements IGodsService {

    @Autowired
    private IGodsDao godsDao;

    @Autowired
    private IOriginsClient originsClient;

    @Override
    public Gods get(Integer id_gods) {
        //return godsDao.findById(id_gods).get();
        Gods gods = godsDao.findById(id_gods).orElse(null);
        if (null != gods) {
            Origins origins = originsClient.getOne(gods.getId_origins());
            gods.setOrigins(origins);
        }
        return gods;
    }

    @Override
    public List<Gods> getAll() {
        return (List<Gods>) godsDao.findAll();
    }

    @Override
    public void post(Gods gods) {
        godsDao.save(gods);
    }

    @Override
    public void put(Gods gods, int id_gods) {
        godsDao.findById(id_gods).ifPresent((x)->{
            gods.setId_gods(id_gods);
            godsDao.save(gods);
        });
    }

    @Override
    public void delete(int id_gods) {
           godsDao.deleteById(id_gods);
    }
}
