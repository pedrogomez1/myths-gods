package com.psgs.mythsgods.controller;

import com.psgs.mythsgods.entity.Gods;
import com.psgs.mythsgods.service.IGodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/gods")
public class GodsController {

    @Autowired
    IGodsService godsService;

    @GetMapping
    public List<Gods> getAllGods() {
        return godsService.getAll();
    }

    @GetMapping("/{id_gods}")
    public Gods getOne(@PathVariable(value = "id_gods") int id_gods) {
        return godsService.get(id_gods);
    }

    @PostMapping
    public void add(@RequestBody Gods gods) {
        godsService.post(gods);
    }

    @PutMapping("/{id_gods}")
    public void update(@RequestBody Gods gods, @PathVariable(value = "id_gods") int id_gods) {
        godsService.put(gods, id_gods);
    }

    @DeleteMapping("/{id_gods}")
    public void delete(Gods gods, @PathVariable(value = "id_gods") int id_gods) {
        godsService.delete(id_gods);
    }

}
