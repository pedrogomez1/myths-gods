package com.psgs.mythsgods.client;

import com.psgs.mythsgods.model.Origins;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "myths-origins")
@RequestMapping("/origins")
public interface IOriginsClient {

    @GetMapping("/{id_origins}")
    public Origins getOne(@PathVariable(value = "id_origins") int id_origins);

}
