package com.psgs.mythsgods.dao;

import com.psgs.mythsgods.entity.Gods;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IGodsDao extends CrudRepository<Gods, Integer> {

}
