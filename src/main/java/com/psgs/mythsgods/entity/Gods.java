package com.psgs.mythsgods.entity;

import com.psgs.mythsgods.model.Origins;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

    @Entity
    @Table(name = "gods")
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public class Gods implements Serializable {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private int id_gods;
        private String name;
        private String title;
        private String lore;
        private int id_origins;

        @Transient
        Origins origins;

    }